describe("BaseRepository", function() {
    beforeEach(module('persistenceJar'));
    
    describe("when an entity is saved", function () { 
        var BaseEntityTest;
        
        beforeEach(inject(function (BaseEntity) {
            BaseEntityTest = BaseEntity;
        }));
        
        it("should be possible to exclude a field", function() {
            BaseEntityTest.add('id', 'string');
            expect(BaseEntityTest.id).toBeDefined();
        });
        
        
    });
    
});