describe("BaseEntity", function() {
    beforeEach(module('persistenceJar'));
    
    describe("when a new field is added", function () { 
        var BaseEntityTest;
        
        beforeEach(inject(function (BaseEntity) {
            BaseEntityTest = BaseEntity;
        }));
        
        it("should be found in BaseEntity properties", function() {
            BaseEntityTest.add('id', 'string');
            expect(BaseEntityTest.id).toBeDefined();
        });
        
        it("should allow a value to be set to it", function () {
            BaseEntityTest.add('id', 'string');
            BaseEntityTest.setFieldValue('id', 'value');
            expect(BaseEntityTest.id).toBe('value');
        });
        
        it("should be found in a simple object when our object is converted", function () {
            BaseEntityTest.add('id', 'string');
            BaseEntityTest.setFieldValue('id', 'value');
            
            expect(BaseEntityTest.toSimpleObject()).toEqual({
                id: 'value'
            });
        });
        
    });
    
    describe("when a validator is attached to a field", function () {
        var BaseEntityTest;
        
        beforeEach(inject(function (BaseEntity, ValidatorsContainer) {
            BaseEntityTest = BaseEntity;
        }));
        
        it("should throw an error if the field name is invalid", function () {
            expect(function () {
                BaseEntityTest.addValidator('id');
            }).toThrow();
        });
        
        it("should throw an error if the specified validator does not exists", function () {
            BaseEntityTest.add('id', 'string');
            
            expect(function () {
                BaseEntityTest.addValidator('id', 'UndefinedValidator');
            }).toThrow();
        });
        
        it("should not throw any error if field name, validator name and validator parameters are well defined", 
            inject(function (ValidatorsContainer, LengthValidator) {
                BaseEntityTest.add('id', 'string');
                
                ValidatorsContainer.register('LengthValidator', LengthValidator);
                
                expect(function () {
                    BaseEntityTest.addValidator('id', 'LengthValidator', {min: 2, max: 5});
                }).not.toThrow();
            })
        );

        it("should add the validator and the group of validations to the object", 
            inject(function (ValidatorsContainer, LengthValidator) {
                BaseEntityTest.add('id', 'string');
                
                // register LengthValidator
                ValidatorsContainer.register('LengthValidator', LengthValidator);
                
                // attache the validator to the id field
                BaseEntityTest.addValidator('id', 'LengthValidator', {min: 2, max: 5});
                
                expect(BaseEntityTest.$$fields['id'].validators['LengthValidator']).toBeDefined();
        }));
        
        it("should be possible to validate the object against the validator", 
            inject(function (ValidatorsContainer, LengthValidator) {
                BaseEntityTest.add('id', 'string');
                
                // register LengthValidator
                ValidatorsContainer.register('LengthValidator', LengthValidator);
                
                // attache the validator to the id field
                BaseEntityTest.addValidator('id', 'LengthValidator', {min: 2, max: 5});
                
                BaseEntityTest.setFieldValue('id', 'good');
                expect(BaseEntityTest.validate()).toBe(true);
                
                BaseEntityTest.setFieldValue('id', 'not so good');
                expect(BaseEntityTest.validate()).toBe(false);
        }));
        
        it("should add an error that has to be found in the object returned by the getLastValidationErrors() method", 
            inject(function (ValidatorsContainer, LengthValidator) {
                BaseEntityTest.add('id', 'string');
                
                // register LengthValidator
                ValidatorsContainer.register('LengthValidator', LengthValidator);
                
                // attache the validator to the id field
                BaseEntityTest.addValidator('id', 'LengthValidator', {min: 2, max: 5});
                BaseEntityTest.setFieldValue('id', 'wrong length value');
                
                BaseEntityTest.validate();
                
                expect(BaseEntityTest.getLastValidationErrors()).toEqual({
                    id: {
                        max: 'The given value is too long.'
                    }
                });
        }));
    });
});