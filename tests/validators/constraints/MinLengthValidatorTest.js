describe("MinLengthValidatorTest", function() {
    describe("when the validator is instantiated", function () { 
        var LengthValidatorTest;
        
        beforeEach(module('persistenceJar'));
        beforeEach(inject(function (ValidatorsContainer, LengthValidator) {
            LengthValidatorTest = new LengthValidator({min: 2, max: 10});
        }));
        
        it('should return true if the given value match the expectation', function () {
            expect(LengthValidatorTest.validate('string')).toBe(true);
        });
        
        it('should return false if the given value does not match the expectation', function () {
            expect(LengthValidatorTest.validate('a long string')).toBe(false);
        });
    });
});
