describe("ValidatorsContainerTest", function() {
    beforeEach(module('persistenceJar'));
    
    describe("when a validator instance is checked", function () { 
        var ValidatorsContainerTest;
        
        beforeEach(inject(function (ValidatorsContainer) {
            ValidatorsContainerTest = ValidatorsContainer;
        }));
        
        it("should throw error if the alias is null or not a string", function() {
            expect(function () { 
                ValidatorsContainerTest.checkValidator('', 'string'); 
            }).toThrow();
        });
        
        it("should throw error if the provided validator is not a function", function () {
            expect(function () { 
                ValidatorsContainerTest.checkValidator('testConstraint', 'string'); 
            }).toThrow(); 
        });
        
        it("should throw error if the provided validator does not contain the `validate` method", function () {
            expect(function () { 
                ValidatorsContainerTest.checkValidator('testValidator', function () {}); 
            }).toThrow(); 
        });
        
        it("should not throw any error if the provided validator is well defined", function () {
            function WellDefinedValidator() {};
            WellDefinedValidator.prototype.validate = function () { return true; };
            
            expect(function () { 
                ValidatorsContainerTest.checkValidator('testValidator', WellDefinedValidator); 
            }).not.toThrow(); 
        });
    });
    
    describe("when a validator is registered", function () {
        var ValidatorsContainerTest;
        
        beforeEach(inject(function (ValidatorsContainer) {
            ValidatorsContainerTest = ValidatorsContainer;
        }));
        
        it("should be stored into the ValidatorsContainer object", function () {
            function WellDefinedValidator() {};
            WellDefinedValidator.prototype.validate = function () { return true; };
            
            ValidatorsContainerTest.register('WellDefinedValidator', WellDefinedValidator); 
            
            expect(ValidatorsContainerTest.hasValidator('WellDefinedValidator')).toBe(true);
        });
    });
});