'use strict';

var persistenceJar;
try {
   persistenceJar = angular.module("persistenceJar");
} catch(err) {
   persistenceJar = angular.module("persistenceJar", []);
}

persistenceJar.factory('LengthValidator', ['ValidatorsContainer', function (ValidatorsContainer) {
    /**
     * Length constraint.
     * 
     * @param {object} options ({Number} min, {Number} max)
     * @returns {LengthValidator}
     */
    function LengthValidator(options) {
        if (undefined === options.min && undefined === options.max) {
            throw new Error('Either option `min` or `max` must be given for LengthValidator');
        }
        
        if (options.min && typeof options.min != 'number') {
            throw new Error('`min` constraint must be a number.');
        }
        
        if (options.max && typeof options.max != 'number') {
            throw new Error('`max` constraint must be a number.');
        }
        
        this.violationMessages = {
            min: 'The given value is too short.',
            max: 'The given value is too long.'
        };
        
        this.violations = {};
        
        this.min = options.min;
        this.max = options.max;
    };
    
    /**
     * Add a violation.
     * 
     * @param {string} option validator option name
     */
    LengthValidator.prototype.addViolation = function (option) {
        this.violations[option] = this.violationMessages[option];
    };
    
    /**
     * Add a violation.
     * 
     * @param {string} option validator option name
     */
    LengthValidator.prototype.getViolations = function () {
        return this.violations;
    };
    
    /**
     * Validate string length to be either longer or shorter than the
     * set value for this validator.
     * 
     * @param {string} value
     * @return {boolean}
     */
    LengthValidator.prototype.validate = function (value) {
        var validity = true;
        
        if (this.min && value.length < this.min) {
            this.addViolation('min');
            validity = false;
        }
        
        if (this.max && value.length > this.max) {
            this.addViolation('max');
            validity = false;
        }
        
        return validity;
    };
    
    ValidatorsContainer.register('Length', LengthValidator);
    
    return LengthValidator;
}]);
