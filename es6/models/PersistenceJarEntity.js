'use strict';

// import {ValidatorsContainer} from '../validators/ValidatorsContainer';


class PersistenceJarEntity {
    constructor (data) {
        /**
         * It contains object fields with information about them (type, enumerable, etc).
         */
        this.$$fields = {};

        /**
         * It contains object fields data. It is used to store fields data given
         * to the contructor. Lately, when a field is added, the add method will
         * check if that fields has a value stored in this.$$data and, if set, it will
         * set it to the field too.
         */
        this.$$data = data || {};

        this.$$pj = {
            validationErrors: {}
        };
    }

    /**
     * Adds a field to object.
     *
     * @param {string} fieldName
     * @param {string} fieldType
     * @param {object} options
     *      - {boolean} enumerable - field represents a collections or not (default to false)
     *      - {string} map - server side name of the field
     *      - {boolean} exclude - to sent or not the field data to the server
     */
    add (fieldName, fieldType, options) {
        var options = options || {};
        var standardDataTypes = ["string", "number", "boolean", "array", "object"];

        // init field data
        var enumerable = (typeof options.enumerable == 'boolean') ? options.enumerable : false;
        var map = (typeof options.map == 'string') ? options.map : fieldName;
        var exclude = options.exclude ? true : false;

        // store field data
        this.$$fields[fieldName] = {
            type: fieldType,
            enumerable: enumerable || fieldType == 'array' ? true : false,
            exclude: exclude,
            map: map,
            validators: {}
        };

        // initialize validators
        var validator;
        if (Array.isArray(options.validators)) {
            for (var i = 0; i < options.validators.length; i++) {
                validator = options.validators[i];
                this.addValidator(fieldName, validator[0], validator[1], validator[2]);
            }
        }

        // initialize field by setting default values like empty array, empty object or null
        if (fieldType == 'array' || enumerable) {
            this[fieldName] = [];
        } else if (fieldType == 'object') {
            this[fieldName] = {};
        } else if (standardDataTypes.indexOf(this.$$fields[fieldName]['type']) == -1) {
            this[fieldName] = null;
        } else {
            this[fieldName] = null;
        }

        // set field value if data was provided. try to use the map to extract the value.
        // if map as key doesn't exist, then use the fieldName to extract the value.
        if (this.$$data[map] !== undefined && this.$$data[map] !== null) {
            this.setFieldValue(fieldName, this.$$data[map]);
        } else if (this.$$data[fieldName] !== undefined && this.$$data[fieldName] !== null) {
            this.setFieldValue(fieldName, this.$$data[fieldName]);
        }
    }

    /**
     * Attach validator to field.
     *
     * @param {string} field
     * @param {string} name
     * @param {object} properties
     * @param {string[]} groups
     *
     * @return {boolean}
     */
    addValidator (field, name, properties, groups) {
        if (!this.$$fields[field]) {
            throw new Error('Field with name `' + field + '` is not defined.');
        }

        if (!ValidatorsContainer.hasValidator(name)) {
            throw new Error('Validator with name `' + name + '` set on the field `' + field + '` is not defined.');
        }

        if (typeof groups === 'undefined') {
            groups = [];
        } else if (!Array.isArray(groups)) {
            throw new Error('Validation groups for the field with name `' + name + '` should be an array of strings.'
                            + '`' + typeof groups + '` given.' );
        }

        this.$$fields[field].validators[name] = { properties: properties, groups: groups };
    }

    /**
     * Sets a field. If the field is enumerable, then the value is added to the
     * field collection, it does not override the field. If the field is a user
     * defined object, then the method tries to initiale a new object with the
     * given value (which should be a simple javascript object).
     *
     * @param {string} fieldName
     * @param {mixed} value
     */
    setFieldValue (fieldName, value) {
        var standardDataTypes = ["string", "number", "boolean", "array", "object"];

        if (standardDataTypes.indexOf(this.$$fields[fieldName]['type']) > -1) {
            this[fieldName] = value;
        } else {
            var customType = this.$$fields[fieldName]['type'];

            if (this.$$fields[fieldName]['enumerable']) {
                for (var i = 0; i < value.length; i++) {
                    var object = new customType(value[i]);
                    this[fieldName].push(object);
                }
            } else {
                var object = new customType(value);
                this[fieldName] = object;
            }
        }
    }

    /**
     * Sets data to object's fields.
     *
     * @param {object} data
     */
    fill (data) {
        for (var field in data) {
            if (data[field] !== undefined) {
                this.setFieldValue(field, data[field]);
            }
        }
    }

    /**
     * Convert an user defined object to a simple javascript object. This method
     * is useful for example to prepare data for sending it to an API.
     *
     * @param {boolean} useMapping If true, the field map is used as key for that field value.
     *
     * @param {boolean} useExclusion - If true, it checks if a field has to be ignored or not
     *                               based on the field configuration.
     *
     * @returns {Object}
     */
    toSimpleObject (useMapping, useExclusion) {
        var getFieldsToSave = function (object) {
            var fields = {}, auxFieldsForArray = [], map;

            for (var field in object.$$fields) {
                if (useExclusion && object.$$fields[field].exclude) continue;

                map = useMapping ? object.$$fields[field].map : field;

                if (Array.isArray(object[field])) {
                    auxFieldsForArray = [];
                    for (var i = 0; i < object[field].length; i++) {
                        if (typeof object[field][i].toSimpleObject == 'function') {
                            auxFieldsForArray.push(object[field][i].toSimpleObject(useMapping, useExclusion));
                        } else {
                            auxFieldsForArray.push(object[field][i]);
                        }
                    }
                    fields[map] = auxFieldsForArray;
                } else if (object[field] && typeof object[field].toSimpleObject == 'function') {
                    fields[map] = object[field].toSimpleObject(useMapping, useExclusion);
                } else {
                    fields[map] = object[field];
                }
            }

            return fields;
        };

        return getFieldsToSave(this);
    }

    /**
     * Validate object.
     * If group is not defined, then all validators attached will be called.
     * Notice: The errors object will be cleared.
     *
     * @param {string} group
     * @returns {boolean}
     */
    validate (group) {
        var isValid = true,
            validatorClass,
            validatorSettings,
            validator;

        if (group && !typeof group == 'string') {
            throw new Error('Invalid group.');
        }

        // to be replaced
        angular.copy({}, this.$$pj.validationErrors);

        var validateSubObject = function (field, group) {
            if (typeof field.toSimpleObject == 'function') {
                return field.validate(group);
            }
            return true;
        };

        for (var field in this.$$fields) {
            // validate subobjects
            if (!validateSubObject(this.$$fields[field], group)) {
                isValid = false;
            }

            // validate enumerable subobjects
            if (Array.isArray(this.$$fields[field])) {
                for (var i = 0; i <= this.$$fields[field].length; i++) {
                    if (!validateSubObject(this.$$fields[field][i], group)) {
                        isValid = false;
                    }
                }
            }

            // validate field
            for (var validatorName in this.$$fields[field].validators) {
                validatorClass = ValidatorsContainer.getValidator(validatorName);

                if (!validatorClass) {
                    continue;
                }

                validatorSettings = this.$$fields[field].validators[validatorName];

                if (group && validatorSettings.indexOf(group) == -1) {
                    continue;
                }

                validator = new validatorClass(validatorSettings.properties);

                if (!validator.validate(this[field])) {
                    this.$$pj.validationErrors[field] = validator.getViolations();
                    isValid = false;
                }
            }
        }

        return isValid;
    }

    /**
     * The errors object is cleaned each time the validate function is called.
     *
     * @returns {object}
     */
    getLastValidationErrors () {
        var errors = this.$$pj.validationErrors;

        var standardDataTypes = ["string", "number", "boolean", "array", "object"];

        // collect errors from subobjects
        for (var field in this.$$fields) {
            if (standardDataTypes.indexOf(this.$$fields[field]['type']) == -1) {
                if (this.$$fields[field]['enumerable']) {
                    for (var i = 0; i <= this[field].length; i++) {
                        errors[field].push(this[field].getLastValidationErrors());
                    }
                } else {
                    errors[field] = this[field].getLastValidationErrors();
                }
            }
        }

        return errors;
    }
}
