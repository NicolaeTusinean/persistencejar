'use strict';

class PersistenceJarRepository {

  constructor (objectType, data) {
    /**
     * Set the class that is represented by this repository.
     * @param {mixed} objectType
     */
    if (!objectType) {
        throw new Error('Object type is required.');
    }

    this.objectType = objectType;

    /**
     * Api URLs
     */
    this.api = {
        baseURL:   '',      // GET    http://domain.tld/model
        createURL: '',      // POST   http://domain.tld/model
        deleteURL: '',      // DELETE http://domain.tld/model/{id}
        indexURL:  '',      // GET    http://domain.tld/model
        updateURL: '',      // PUT    http://domain.tld/model/{id}
        showURL:   '',      // GET    http://domain.tld/model/{id}
        searchURL: ''       // POST    http://domain.tld/model/search
    };

  }

  /**
   * Find object by id.
   *
   * @param {string|int} id
   * @returns {object}
   */
  find (id, options) {
      var options = options || {},
          self    = this;

      var request = {
          method: 'GET',
          url: this.api.showURL.replace('{id}', id).replace(':id', id)
      };

      if (options['method']) {
          request['method'] = options['method'];
      }

      if (options['url']) {
          request['url'] = options['url'].replace('{id}', id).replace(':id', id);
      }

      let req = new Request();
      return req[options['method']](options['url']).then(function (response) {
          return new this.objectType(response.data);
      });
  }


  /**
   * Find by criteria
   *
   * @param {object} criteria (key value pair)
   * @param {object} sort (field: 1 / -1)
   * @param {object} limit
   * @returns {mixed}[]
   */
/*
  findBy (criteria, sort, limit) {
      var request = {
          method: 'POST',
          url: this.api.searchURL,
          data: {
              criteria: criteria ? criteria : {},
              sort: sort ? sort : {},
              limit: limit ? limit : {}
          }
      };

      return $http(request).then(function (response) {
          var objects = [], newObject = null;
          for (var i = 0; i < response.data.length; i++) {
              newObject = new this.objectType(response.data[i]);
              objects.push(newObject);
          }

          return objects;
      });

  }
*/

  /**
   * Gets all instances.
   *
   * @param {object} options
   * @returns {mixed}[]
   */
  all (options) {
      var options = options || {},
          self    = this;

      var request = {
          method: 'GET',
          url:    this.api.indexURL
      };

      if (options['method']) {
          request['method'] = options['method'];
      }

      if (options['url']) {
          request['url'] = options['url'];
      }

      let req = new Request();
      let that = this;
      return req[request.method.toLowerCase()](request.url).then(function (response) {
          var objects = [], newObject = null;
          for (var i = 0; i < response.data.length; i++) {
              newObject = new that.objectType(response.data[i]);
              objects.push(newObject);
          }

          return objects;
      });
  }

  /**
   * Delete instance.
   *
   * @param {string|id} id
   * @returns {mixed}
   */
  delete (id, options) {
      var options = options || {};

      var request = {
          method: 'DELETE',
          url: this.api.deleteURL.replace('{id}', id).replace(':id', id)
      };

      if (options['method']) {
          request['method'] = options['method'];
      }

      if (options['url']) {
          request['url'] = options['url'].replace('{id}', id).replace(':id', id);
      }

      let req = new Request();
      return req[options['method']](options['url']);
  }

  /**
   * Exclude fields from object. This function makes sens when an object is
   * saved and there is a need to exclude some of its fields.
   *
   * @param {object} fieldsToSave
   * @param {object} excludedFields
   */
  excludeFields (fieldsToSave, excludedFields) {
      if (excludedFields && Array.isArray(excludedFields)) {
          var excludedSubFields,
              excludedField,
              excludedFieldValue;

          for (var i = 0; i < excludedFields.length; i++) {
              excludedField = excludedFields[i];
              excludedSubFields = excludedField.split('.');

              if (excludedSubFields.length > 0) {
                  excludedFieldValue = fieldsToSave[excludedField];

                  for (var j = 1; j < excludedSubFields.length; j++) {
                      if (j === excludedSubFields[j].length - 1) {
                          delete excludedFieldValue[excludedSubFields[j]];
                      } else {
                          excludedFieldValue = excludedFieldValue[excludedSubFields[j]];
                      }
                  }
              } else {
                  delete fieldsToSave[excludedField];
              }
          }
      }
  }

  /**
   * Saves an object.
   *
   * @param {mixed} object
   *
   * @param {object} options
   *      - {array}  excludedFields - array of fieldnames that will be excluded from request
   *      - {string} method  - use GET, DELETE, POST, PUT instead of PUT
   *      - {string} wrapper - wrap data within one object with the given name
   *
   * @returns {}
   */
  save (object, options) {
      var fieldsToSave   = {},
          options        = options || {},
          excludedFields = [],
          extraFields    = [];

      fieldsToSave = object.toSimpleObject(true, true);

      excludeFields(fieldsToSave, options['excludedFields']);

      if (options['extraFields']) {
          angular.extend(fieldsToSave, options['extraFields']);
      }

      var request = {
          method: 'PUT',
          url:    this.api.updateURL,
          data:   fieldsToSave
      };

      if (options['method']) {
          request['method'] = options['method'];
      }

      if (options['url']) {
          request['url'] = options['url'];
      }

      if (options['wrapper']) {
          var obj = {};
          obj[options['wrapper']] = fieldsToSave;
          request['data'] = obj;
      }

      let req = new Request();
      return req[options['method']](options['url']);
  }

  /**
   * Defines Api URL. You can redefine each of the URLs from the api object directly:
   *    this.api.createURL
   *
   * @param {string} url
   */
  setApiURL (url) {
      var hasTrailingSlash = url[url.length - 1] == '/';

      this.api.baseURL   = url;
      this.api.createURL = this.api.baseURL;
      this.api.deleteURL = this.api.baseURL + (hasTrailingSlash ? '{id}' : '/{id}');
      this.api.indexURL  = this.api.baseURL;
      this.api.updateURL = this.api.baseURL + (hasTrailingSlash ? '{id}' : '/{id}');
      this.api.showURL   = this.api.baseURL + (hasTrailingSlash ? '{id}' : '/{id}');
      this.api.searchURL = this.api.baseURL + (hasTrailingSlash ? 'search' : '/search');
  }
}
