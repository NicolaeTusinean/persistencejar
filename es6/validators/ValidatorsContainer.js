'use strict';

class ValidatorsContainer {
  constructor (data) {
      /**
       * It contains object fields with information about them (type, enumerable, etc).
       */
      this.validators = {};

      this.checkValidator = function (alias, validator) {
          if (alias == '' || typeof alias != 'string') {
              throw new Error('Provided alias should be a non-empty string.');
          }

          if (typeof validator != 'function') {
              throw new Error('Provided validator `' + alias + '` is not a JavaScript function.');
          }

          if (typeof validator.prototype.validate != 'function') {
              throw new Error('Provided validator `' + alias + '` does not implement the `validate` method.');
          }
      };
  }

  /**
   * Get a validator by name.
   *
   * @param {string} name
   * @returns {Validator}
   */
  getValidator (name) {
      if (this.hasValidator(name)) {
          return this.validators[name];
      }

      return null;
  }

  /**
   * Check if validator exists.
   *
   * @param {string} name
   * @returns {Boolean}
   */
  hasValidator (name) {
      if (typeof name !== 'string') {
          return false;
      }

      return this.validators[name] ? true : false;
  }

  /**
   * Register a validator like a collection of validators to be used for other
   * objects of this microframework.
   *
   * @param {string} name
   * @param {function} validator
   */
  register (name, validator) {
      this.checkValidator(name, validator);
      this.validators[name] = validator;
  }
}
