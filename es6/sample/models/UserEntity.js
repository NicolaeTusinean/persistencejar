class User extends PersistenceJarEntity {
  constructor (data) {
    super(data);

    this.add('id', 'number');
    this.add('username', 'string');
    this.add('firstname', 'string');
    this.add('lastname', 'string');
    this.add('favoriteNumbers', 'array', {map: 'favorite-numbers'});
  }
}
