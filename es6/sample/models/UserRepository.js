class UserRepository extends PersistenceJarRepository {
  constructor(data) {
    super(User, data);

    // set api url for this model
    this.setApiURL('/data/users');

    // overwrite indexURL api url
    this.api.indexURL = 'data/users.json';
  }
}
