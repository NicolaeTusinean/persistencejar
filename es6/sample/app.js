// console.log('\n\n\n\n\n\n\n');
// console.log('Example of how to initialize an existing object. Check the object properties.');
let user = new User({
  'id': 1,
  'username': 'john',
  'firstname': 'john',
  'lastname': 'doe',
  'favoriteNumbers': [1, 2, 3]
});
console.log(user);

// console.log('\n\n\n\n\n\n\n');
// console.log('Example of how to get API objects using the Repository class');
let userRepository = new UserRepository();
userRepository.all().then(function (users) {
  console.log(users);
});
