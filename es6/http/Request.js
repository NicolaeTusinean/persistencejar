class Request {
  constructor () {
    let availableMethods = [
      'GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS'
    ];

    let that = this;

    availableMethods.forEach(function (item, index, array) {
      that[item.toLowerCase()] = function (url) {
        return this.doAjaxRequest(item, url);
      }
    });
  }

  doAjaxRequest (method, url) {
    return new Promise(function(resolve, reject) {

      var req = new XMLHttpRequest();
      req.open(method, url);

      var response = {};
      req.onload = function() {
        response['code'] = req.status;

        if (req.status == 200) {
          // Resolve the promise with the response text
          response['data'] = JSON.parse(req.response);
          resolve(response);
        } else {
          // Otherwise reject with the status text
          response['error'] = req.statusText;
          reject(response);
        }


      };

      req.onerror = function() {
        reject(Error("Network Error"));
      };

      // Make the request
      req.send();
    });
  }
}
