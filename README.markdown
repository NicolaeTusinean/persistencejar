# README #

PersistenceJar is a micro framework for AngularJS that comes with a persistence layer for JavaScript objects. It is pretty similar to an ORM/ODM but it maps objects to API.
Using PersistenceJar, developement time gets shorter, the code becomes cleaner and more readable, good practices are encouraged. 

### What is this repository for? ###

* PersistenceJar offers a base persistence layer for JavaScript objects.
* PersistenceJar v0.8

### Documentation ###

* Please go to: http://www.nicolaetusinean.ro/persistencejar/

### Roadmap ###
v0.9 - Response Interceptor / HTTP Adapters (enable hypermedia)
Allow the configuration of the server response.
v1.0 
 - Allow built-in object to be set up as field type (e.g.: Date, Math)